import yaml
import pandas as pd
import requests
import sqlalchemy
from time import sleep
import datetime
from math import ceil
from tqdm import tqdm
import calendar


class Metrika():
    def __init__(self):

        self.loadConfig()
        self.__set_conn()

    def loadConfig(self, file_config='../config/config.yaml'):
        with open(file_config, 'r') as stream:
            try:
                cfg = yaml.safe_load(stream)
                self.__db_user = cfg['DB_PG']['user']
                self.__db_pass = cfg['DB_PG']['pass']
                self.__db_host = cfg['DB_PG']['host']
                self.__db_db = cfg['DB_PG']['db']
                self.__db_table_days = cfg['Metrika']['db_table_days']
                self.__db_table_weeks = cfg['Metrika']['db_table_weeks']
                self.__db_table_months = cfg['Metrika']['db_table_months']
                accounts = cfg['Metrika']['accounts']
                self.__accounts = []
                for account in accounts.keys():
                    self.__accounts.append({'login': account, 'token': accounts[account]['token'],
                                            'cert_path': accounts[account]['cert_path'],
                                            'ids': accounts[account]['ids']})
            except Exception as exc:
                print(exc)

    def __date_pairs(self, from_date, to_date, interval):
        # дописать деление на интервалы первых и последних дней, когда даты приходятся не четкие
        result = []
        if interval='week':
            start = datetime.datetime.strptime(from_date, '%Y-%m-%d')
            start = start - datetime.timedelta(days=start.weekday())
            end = datetime.datetime.strptime(to_date, '%Y-%m-%d')
            end = end + timedelta(days=5 - (end.weekday()))

        if start == end:
            print(1)
            print(start.strftime('%Y-%m-%d') + ' ' + end.strftime('%Y-%m-%d'))
        else:
            while True:
                after = start
                if interval == 'month':
                    after = after.replace(
                        day=list(calendar.monthrange(start.year, start.month))[1])
                elif interval == 'week':
                    while after.isoweekday() != 7:
                        after += datetime.timedelta(days=1)
                elif interval == 'year':
                    after = after.replace(day=31, month=12)
                elif interval == 'quarter':
                    while not ((after.day == 31 and after.month == 3)
                               or (after.day == 30 and after.month == 6)
                               or (after.day == 30 and after.month == 9)
                               or (after.day == 31 and after.month == 12)):
                        after += datetime.timedelta(days=1)
                elif interval == 'LAST_SUNDAY_OF_YEAR':
                    after = after.replace(day=31, month=12)
                    while after.isoweekday() != 6:
                        after -= datetime.timedelta(days=1)
                    if after < start:
                        after = after.replace(day=31, month=12, year=after.year + 1)
                        while after.isoweekday() != 6:
                            after -= datetime.timedelta(days=1)
                if after < end:
                    result.append([start.strftime('%Y-%m-%d'), after.strftime('%Y-%m-%d')])
                    start = after
                    start += datetime.timedelta(days=1)
                else:
                    result.append([start.strftime('%Y-%m-%d'), end.strftime('%Y-%m-%d')])
                    break
        return result

    def getData(self,
                from_date=(datetime.date.today() - datetime.timedelta(days=1)),
                to_date=(datetime.date.today()),
                filling=False,
                group='day'
                ):

        if pd.to_datetime(from_date) > pd.to_datetime(to_date):
            if self.max_date < pd.to_datetime(to_date):
                from_date = pd.to_datetime(self.max_date).strftime('%Y-%m-%d')[0]

        # Нужно ли наполнять от последней даты
        if filling:
            # если пусто, то берем за вчера
            self.__get_max_date()
            if pd.isnull(self.max_date[0]) == False:
                from_date = self.max_date[0].strftime('%Y-%m-%d')

            to_date = (datetime.date.today())

        datePairs = self.__date_pairs(from_date, to_date, group)

        if group == 'day':
            group = {"group": 'day'}
            dimensions = {'dimensions': "ym:s:date"}
        elif group == 'week':
            group = {"group": 'week'}
            dimensions = {'dimensions': "ym:s:startOfWeek"}
        else:
            group = {"group": 'month'}
            dimensions = {'dimensions': "ym:s:startOfMonth"}

        metrics = {
            'metrics': "ym:s:users,ym:s:visits,ym:s:pageviews,ym:s:bounceRate,ym:s:pageDepth,ym:s:avgVisitDurationSeconds"}

        url = 'https://api-metrika.yandex.net/stat/v1/data'
        filters = [
            {'name': 'None', "filter": ""}

            # {'name': 'turbo:True', "filter": "ym:pv:isTurboPage=='yes'"}, \
            # {'name': 'turbo:False', "filter": "ym:pv:isTurboPage=='no'"}, \
            # {'name': 'source:yandex', "filter": "ym:s:<attribution>SearchEngine=~'(.*)yandex(.*)'"},
            # {'name': 'source:google', "filter": "ym:s:<attribution>SearchEngine=~'(.*)google(.*)'"},
            # # 1 — десктоп, 2 — мобильные телефоны, 3 — планшеты, 4 — TV
            # {'name': 'device:desktop', "filter": "(ym:s:deviceCategory=='desktop') and (ym:pv:isTurboPage=='no')"}, \
            # {'name': 'device:mobile', "filter": "(ym:s:deviceCategory=='mobile') and (ym:pv:isTurboPage=='no')"}, \
            # {'name': 'source:yandex_zen', "filter": "(ym:s:lastSignRecommendationSystem=='zen_yandex')"}, \
            # {'name': 'source:google_discover', "filter": "(ym:s:3AlastSignRecommendationSystem=='google_discover')"}, \
            # {'name': 'utm_source:yxnews', "filter": "ym:pv:UTMSource=~'(.*)yxnews(.*)'"}, \
            # {'name': 'utm_source:directadvert+nnn',
            #  "filter": "(ym:pv:UTMSource=~'(.*)directadvert(.*)') OR (ym:pv:UTMSource=~'(.*)nnn(.*)')"}, \
            # {'name': 'utm_source:mirtesen', "filter": "ym:pv:UTMSource=~'(.*)mirtesen(.*)'"}, \
            # {'name': 'utm_source:smi2', "filter": "ym:pv:UTMSource=~'(.*)smi2(.*)'"},
            # {'name': 'utm_source:gnezdo', "filter": "ym:pv:UTMSource=~'(.*)gnezdo(.*)'"}, \
            # {'name': 'utm_source:infox', "filter": "ym:pv:UTMSource=~'(.*)infox(.*)'"},
            # {'name': 'utm_source:24smi', "filter": "ym:pv:UTMSource=~'(.*)24smi(.*)'"},
            # {'name': 'utm_source:pulse', "filter": "ym:pv:UTMSource=~'(.*)pulse(.*)'"}
            # # {'name':'utm_source:lentainform',"filter":"ym:pv:UTMSource=~'(.*)lentainform(.*)'"}

        ]
        for i in filters:
            i.update(metrics)
            i.update(dimensions)
            i.update(group)

        parseData = []
        for dateStart, dateEnd in datePairs:
            for account in self.__accounts:
                for site_name, site_id in account['ids'].items():
                    for fltr in filters:
                        parseData.append({"dateStart": dateStart, "dateEnd": dateEnd,
                                          'site_name': site_name, 'site_id': site_id,
                                          'filter': fltr})

        metrika = pd.DataFrame()
        n = 0
        pbar = tqdm(total=len(parseData), leave=True, desc='Download data')
        try:
            for parse in parseData:
                pbar.set_description("{dateStart}:{dateEnd} - {site_name}:[{fltr_name}]". \
                                     format(dateStart=parse['dateStart'],
                                            dateEnd=parse['dateEnd'],
                                            site_name=parse['site_name'],
                                            fltr_name=parse['filter']['name']))
                n += 1
                api_param = {
                    "ids": parse['site_id'],
                    "metrics": parse['filter']['metrics'],
                    "dimensions": parse['filter']['dimensions'],
                    "date1": parse['dateStart'],
                    "date2": parse['dateEnd'],
                    "filters": parse['filter']['filter'],
                    "group": parse['filter']['group'],
                    # "sort": "ym:s:date",
                    "accuracy": "full",
                    "limit": 100000
                }
                header_params = {
                    'GET': '/management/v1/counters HTTP/1.1',
                    'Host': 'api-metrika.yandex.net',
                    'Authorization': 'OAuth ' + TOKEN,
                    'Content-Type': 'application/x-yametrika+json'
                }
                chunks = [metrika, self.__get_data_from_api(api_param, header_params, parse)]
                metrika = pd.concat(chunks, ignore_index=True)
                pbar.update(1)
                # print((dateStart, dateEnd), len(datePairs),datePairs )
                if n == len(datePairs):
                    pbar.close()
                else:
                    sleep(5)
        except Exception as e:
            print(parse, '\n')
            print(e)
            # print(len(parseData))
            # parseData=resetParse(parseData,parse['dateStart'],parse['dateEnd'], parse['site_name'],parse['filter']['name'])
            print('please restart')

        metrika['date'] = pd.to_datetime(metrika['date'])
        metrika = metrika.drop_duplicates()
        self.stat = metrika
        self.group = list(group.values())[0]
        print('ok')
        metrika.tail(10)

    def __get_data_from_api(self, api_param, header_params, parse):

        # Отправляем get request (запрос GET)
        response = requests.get(
            url='https://api-metrika.yandex.net/stat/v1/data',
            params=api_param,
            headers=header_params
        )
        try:
            sleep(5)
            if 'data' in response.json():
                result = response.json()
                result = result['data']
            else:
                sleep(10)
                response = requests.get(
                    self.url,
                    params=api_param,
                    headers=header_params
                )
                sleep(20)
                result = response.json()
                result = result['data']
            # Создаем пустой dict (словать данных)
            dict_data = {}
            # Парсим исходный list формата Json в dictionary (словарь данных)
            for i in range(0, len(result)):
                dict_data[i] = {
                    'date': result[i]["dimensions"][0]["name"],
                    'domain': parse['site_name'],
                    'users': result[i]["metrics"][0],
                    'visits': result[i]["metrics"][1],
                    'pageviews': result[i]["metrics"][2],
                    'bounceRate': result[i]["metrics"][3],
                    'pageDepth': result[i]["metrics"][4],
                    'avgVisitDurationSeconds': result[i]["metrics"][5]
                }
        except Exception as e:
            print(e)
            print(parse)
            print(response.json())
            # print(i,":",len(dict_data[i]))

        # Создаем DataFrame из dict (словаря данных или массива данных)
        # print(i,":dd:",dict_data)
        try:
            dict_keys = dict_data[0].keys()
        except:
            dict_keys = dict_data.keys()

        df = pd.DataFrame.from_dict(dict_data, orient='index', columns=dict_keys)
        df['filter'] = parse['filter']['name']

        fltr = parse['filter']['filter']
        if "tv" in fltr:
            device = 4
        elif "tablet" in fltr:
            device = 3
        elif "mobile" in fltr:
            device = 2
        elif "desktop" in fltr:
            device = 1
        else:
            device = 0
        df['device'] = device
        df['device'] = df['device'].astype(bool)

        if "isTurboPage=='yes'" in fltr:
            turbo = 2
        elif "isTurboPage=='no'" in fltr:
            turbo = 1
        else:
            turbo = 0
        df['turbo'] = turbo
        df['turbo'] = df['turbo'].astype(bool)

        return df

    def pushData(self):
        try:
            size = 15000
            how = 'append'

            if (self.group == 'day'):
                table = self.__db_table_days
            elif self.group == 'week':
                table = self.__db_table_weeks
            else:
                table = self.__db_table_months

            if len(self.stat) <= size:
                self.stat.to_sql(con=self.conn, name=table, if_exists=how, index=False)
            else:
                itr = ceil(len(self.stat) / size)
                n = 0
                for i in range(0, itr):
                    new_size = n + size
                    print("Insert to DB", [n, new_size])
                    self.stat[n:new_size].to_sql(con=self.conn, name=table, if_exists=how, index=False)

                    n += size
            print('Inserted')
        except Exception as e:
            print(e.orig)

    def __set_conn(self):
        try:
            self.conn = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                                 format(self.__db_user,
                                                        self.__db_pass,
                                                        self.__db_host,
                                                        self.__db_db))
        except Exception as e:
            print(e.orig)


y = Metrika()
y.getData(from_date='2022-04-20', to_date='2022-04-30', group='week')
y.pushData()
print(y.stat)
