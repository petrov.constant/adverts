import yaml
import pandas as pd
import requests
import sqlalchemy
from time import sleep
from math import ceil
import datetime
#from tqdm import tqdm


class Luckyads():
    def __init__(self, file_config='../config/config.yaml'):
        self.loadConfig(file_config)
        self.__set_conn()
        self.__get_max_date()
        self.__url = 'https://api.luckyads.pro/v2/webmaster/stats?'

    def getData(self,
                from_date=(datetime.date.today() - datetime.timedelta(days=1)),
                to_date=(datetime.date.today()),
                filling=False
                ):

        if pd.to_datetime(from_date) > pd.to_datetime(to_date):
            if self.max_date < pd.to_datetime(to_date):
                from_date = pd.to_datetime(self.max_date).strftime('%Y-%m-%d')[0]

        # Нужно ли наполнять от последней даты
        if filling:
            # если пусто, то берем за вчера
            self.__get_max_date()
            if pd.isnull(self.max_date[0]) == False:
                from_date = self.max_date[0].strftime('%Y-%m-%d')

            to_date = (datetime.date.today())

            # реализовать различные варианты накатывания обновлений

        filters = 'filters[date_from]={from_date}&\
            filters[date_to]={date_to}&\
            groups[0]=date&\
            groups[1]=site_id&\
            groups[2]=block_id&\
            order_by[0][column]=shows_count&\
            order_by[0][direction]=desc'.format(from_date=from_date, date_to=to_date)

        self.stat = pd.DataFrame()
        url = ''.join((self.__url + filters).split())

        for account in self.__accounts:
            good_response = False
            i = 1
            while good_response == False:
                r = requests.get(url, headers={'Private-Token': account['token']})
                if r.status_code != 200:
                    print('Error request ({}): {} - {}'.format(i, r.status_code, r.content.decode('utf-8')))
                    if (i >= 10):
                        print('No response')
                        exit()
                    sleep(10)
                    i += 1
                else:
                    data = r.json()
                    good_response = True

            luckyads = pd.json_normalize(data['data']['result'])

            luckyads_sites = pd.json_normalize(data['names']['site_id']).T.reset_index()
            luckyads_blocks = pd.json_normalize(data['names']['block_id']).T.reset_index()
            luckyads_sites.columns = ['site_id', 'domain']
            luckyads_blocks.columns = ['block_id', 'block_name']

            luckyads_blocks['block_id'] = luckyads_blocks['block_id'].astype('int')
            luckyads_sites['site_id'] = luckyads_sites['site_id'].astype('int')

            luckyads['date'] = pd.to_datetime(luckyads['date'], format='%Y-%m-%d')
            luckyads['site_id'] = luckyads['site_id'].astype('int')
            luckyads['block_id'] = luckyads['block_id'].astype('int')
            luckyads['referrals_count'] = luckyads['referrals_count'].astype('int')
            luckyads['shows_count'] = luckyads['shows_count'].astype('int')
            luckyads['confirmed_shows_count'] = luckyads['confirmed_shows_count'].astype('int')
            luckyads['clicks_count'] = luckyads['clicks_count'].astype('int')
            luckyads['ctr'] = luckyads['ctr'].astype('float')
            luckyads['confirmed_ctr'] = luckyads['confirmed_ctr'].astype('float')
            luckyads.rename({"money": "profit"}, inplace=True, axis=1)

            block_types = self.__setBlockTypes(luckyads_blocks['block_name'])
            luckyads_blocks = luckyads_blocks.join(block_types)
            luckyads = luckyads.copy().merge(luckyads_sites, on=['site_id'], how='left')
            luckyads = luckyads.copy().merge(luckyads_blocks, on=['block_id'], how='left')
            # del luckyads['id']
            luckyads.drop_duplicates(inplace=True)
            luckyads.sort_values(by=['date'], ascending=False, inplace=True)
            luckyads.reset_index(drop=True, inplace=True)
            luckyads['account'] = account['login']
            chunks = [self.stat, luckyads]
            self.stat = pd.concat(chunks, ignore_index=True)
            sleep(6)

        if (pd.to_datetime(from_date) <= self.max_date):
            t = sqlalchemy.text('DELETE FROM {} WHERE date>=\'{}\''.format(self.__db_table, str(from_date)))
            ##дописать
            self.conn.execute(t)
        print('Loaded')

    def loadConfig(self, file_config):
        with open(file_config, 'r') as stream:
            try:
                cfg = yaml.safe_load(stream)
                self.__db_user = cfg['DB_PG']['user']
                self.__db_pass = cfg['DB_PG']['pass']
                self.__db_host = cfg['DB_PG']['host']
                self.__db_db = cfg['DB_PG']['db']
                self.__db_table = cfg['Luckyads']['db_table']
                accounts = cfg['Luckyads']['accounts']
                self.__accounts = []
                for account in accounts.keys():
                    self.__accounts.append({'login': account, 'token': accounts[account]['token']})
            except Exception as exc:
                print(exc)

    def __set_conn(self):
        try:
            self.conn = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                                 format(self.__db_user,
                                                        self.__db_pass,
                                                        self.__db_host,
                                                        self.__db_db))
        except Exception as e:
            print(e.orig)

    def __setBlockTypes(self, names):
        turbo = []
        amp = []
        florad = []
        mobile = []
        desktop = []
        stick = []
        fullscreen = []
        video = []
        side = []
        main = []

        for item in names:
            item = item.lower()
            if ('турбо' in item) or ('turbo' in item):
                turbo.append(True)
            else:
                turbo.append(False)
            if ('amp' in item) or ('амп' in item):
                amp.append(True)
            else:
                amp.append(False)
            if ('floor' in item) or ('floar' in item):
                florad.append(True)
            else:
                florad.append(False)
            if (':m' in item) or ('mobile' in item) or ('мобил' in item):
                mobile.append(True)
            else:
                mobile.append(False)
            if (':d' in item) or ('desk' in item):
                desktop.append(True)
            else:
                desktop.append(False)
            if ('stick' in item):
                stick.append(True)
            else:
                stick.append(False)
            if ('fullsc' in item):
                fullscreen.append(True)
            else:
                fullscreen.append(False)
            if ('video' in item):
                video.append(True)
            else:
                video.append(False)
            if ('side' in item):
                side.append(True)
            else:
                side.append(False)
            if ('main' in item):
                main.append(True)
            else:
                main.append(False)

        df = pd.DataFrame(list(zip(turbo, amp, florad, mobile, desktop, stick,
                                   fullscreen, video, side, main)),
                          columns=['turbo', 'amp', 'florad', 'mobile', 'desktop',
                                   'stick', 'fullscreen', 'video', 'side', 'main'])

        return df

    def __get_max_date(self):
        try:
            t = sqlalchemy.text('SELECT max(date) FROM {}'.format(self.__db_table))
            self.max_date = pd.to_datetime(self.conn.execute(t).fetchall()[0])
        except Exception as e:
            print(e.orig)

    def pushData(self):
        try:
            size = 15000
            how = 'append'

            if len(self.stat) <= size:
                self.stat.to_sql(con=self.conn, name=self.__db_table, if_exists=how, index=False)
            else:
                itr = ceil(len(self.stat) / size)
                n = 0
                for i in range(0, itr):
                    new_size = n + size
                    print("Insert to DB", [n, new_size])
                    self.stat[n:new_size].to_sql(con=self.conn, name=self.__db_table, if_exists=how, index=False)
                    n += size
            print('Inserted')
        except Exception as e:
            print(e.orig)


lucky = Luckyads()
lucky.getData(filling=True)
lucky.pushData()
