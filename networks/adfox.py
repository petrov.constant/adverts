import yaml
import pandas as pd
import requests
import sqlalchemy
from time import sleep
from math import ceil
import datetime
from tqdm import tqdm


class Adfox():
    def __init__(self):
        self.loadConfig()
        self.__set_conn()
        self.__get_max_date()

    def loadConfig(self, file_config='../config/config.yaml'):
        with open(file_config, 'r') as stream:
            try:
                data = yaml.safe_load(stream)
                self.__accounts = list(data['ADFOX']['accounts'].keys())
                self.cfg = {}
                for account in self.__accounts:
                    self.cfg[account] = {'key': data['ADFOX']['accounts'][account]['key'],
                                         'client_id': data['ADFOX']['accounts'][account]['client_id'],
                                         'owner_id': data['ADFOX']['accounts'][account]['owner_id'],
                                         'sites': data['ADFOX']['accounts'][account]['sites']}
                self.__db_user = data['DB_PG']['user']
                self.__db_pass = data['DB_PG']['pass']
                self.__db_host = data['DB_PG']['host']
                self.__db_db = data['DB_PG']['db']
                self.__db_table = data['ADFOX']['db_table']
            except Exception as e:
                print(e.args)

    def __setBlockTypes(self, names):
        turbo = []
        amp = []
        florad = []
        mobile = []
        desktop = []
        stick = []
        fullscreen = []
        video = []
        side = []
        main = []

        for item in names:
            item = item.lower()
            if ('турбо' in item) or ('turbo' in item):
                turbo.append(True)
            else:
                turbo.append(False)
            if ('amp' in item) or ('амп' in item):
                amp.append(True)
            else:
                amp.append(False)
            if ('floor' in item) or ('floar' in item):
                florad.append(True)
            else:
                florad.append(False)
            if (':m' in item) or ('mobile' in item) or ('мобил' in item):
                mobile.append(True)
            else:
                mobile.append(False)
            if (':d' in item) or ('desk' in item):
                desktop.append(True)
            else:
                desktop.append(False)
            if ('stick' in item):
                stick.append(True)
            else:
                stick.append(False)
            if ('fullsc' in item):
                fullscreen.append(True)
            else:
                fullscreen.append(False)
            if ('video' in item):
                video.append(True)
            else:
                video.append(False)
            if ('main' in item):
                main.append(True)
            else:
                main.append(False)
            if ('side' in item):
                side.append(True)
            else:
                side.append(False)

        types = pd.DataFrame(list(zip(turbo, amp, florad, mobile, desktop,
                                      stick, fullscreen, video, side, main)),
                             columns=['turbo', 'amp', 'florad', 'mobile', 'desktop',
                                      'stick', 'fullscreen', 'video', 'side', 'main'])
        return types

    def getData(self,
                from_date=(datetime.date.today() - datetime.timedelta(days=1)),
                to_date=(datetime.date.today()),
                filling=False):

        if pd.to_datetime(from_date) > pd.to_datetime(to_date):
            if self.max_date < pd.to_datetime(to_date):
                from_date = pd.to_datetime(self.max_date).strftime('%Y-%m-%d')[0]

        self.stat = pd.DataFrame()
        total = 0

        # Нужно ли наполнять от последней даты
        if filling:
            # если пусто, то берем за вчера
            self.__get_max_date()
            if pd.isnull(self.max_date[0]) == False:
                from_date = self.max_date[0]

            to_date = (datetime.date.today())

        for account in self.__accounts:
            total += len(self.cfg[account]['sites'])
        pbar = tqdm(desc='Обработка сайтов', total=total)

        for account in self.__accounts:
            adFox = pd.DataFrame()
            headers = {"X-Yandex-API-Key": self.cfg[account]['key']}
            for site in self.cfg[account]['sites']:
                params = {'name': 'headerBiddingDaily',
                          'client_id': self.cfg[account]['client_id'],
                          'owner_id': self.cfg[account]['owner_id'],
                          'level': 'site',
                          'siteId': site,
                          'dateFrom': pd.to_datetime(from_date).strftime('%Y-%m-%d'),
                          'dateTo': pd.to_datetime(to_date).strftime('%Y-%m-%d')
                          }

                urlGetReport = "https://adfox.yandex.ru/api/report/site"

                r = requests.get(urlGetReport, headers=headers, params=params)

                taskId = r.json()['result']["taskId"]

                urlGetTask = 'https://adfox.yandex.ru/api/report/result?taskId={taskId}'.format(taskId=taskId)
                r = requests.get(urlGetTask, headers=headers)

                req_df = pd.DataFrame(r.json())

                counter = 1
                while req_df.loc['state'].result != 'SUCCESS':
                    r = requests.get(urlGetTask, headers=headers)
                    req_df = pd.DataFrame(r.json())
                    sleep(3)
                    counter += 1

                fields = req_df.loc['fields'].result
                table = pd.DataFrame(req_df.loc['table'].result)
                chunks = [adFox, table]
                adFox = pd.concat(chunks, ignore_index=True)
                pbar.update(1)

            adFox.columns = fields
            adFox['account'] = account
            adFox['date'] = pd.to_datetime(adFox['date'])
            block_types = self.__setBlockTypes(names=adFox['placeName'])
            adFox = adFox.join(block_types)
            adFox['place'] = [i.lower() for i in adFox['placeName']]
            adFox['placeName'] = adFox['place'].astype('category')
            adFox.rename({"siteName": "domain", 'hbWinBidsSum': "profit"}, axis=1, inplace=True)
            adFox['profit'] = adFox['profit'].astype('float')
            adFox['domain'] = [i.lower() for i in adFox['domain']]
            adFox['domain'] = adFox['domain'].astype('category')
            del adFox['place']
            # del adFox['id']
            adFox.drop_duplicates(inplace=True)
            adFox.sort_values(by=['date'], ascending=False, inplace=True)
            adFox.reset_index(drop=True, inplace=True)
            if len(self.stat) > 0:
                chunks = [self.stat, adFox]
                self.stat = pd.concat(chunks, ignore_index=True)
            else:
                self.stat = adFox

        if (pd.to_datetime(from_date) <= self.max_date):
            t = sqlalchemy.text('DELETE FROM {} WHERE date>=\'{}\''.format(self.__db_table, str(from_date)))
            ##дописать
            self.conn.execute(t)

        pbar.close()

    def __set_conn(self):
        try:
            self.conn = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                                 format(self.__db_user,
                                                        self.__db_pass,
                                                        self.__db_host,
                                                        self.__db_db))
        except Exception as e:
            print(e.orig)

    def __get_max_date(self):
        try:
            t = sqlalchemy.text('SELECT max(date) FROM {}'.format(self.__db_table))
            self.max_date = pd.to_datetime(self.conn.execute(t).fetchall()[0])
        except Exception as e:
            print(e.orig)

    def pushData(self):
        try:
            size = 15000
            how = 'append'
            if len(self.stat) <= size:
                self.stat.to_sql(con=self.conn, name=self.__db_table, if_exists=how, index=False)
            else:
                itr = ceil(len(self.stat) / size)
                n = 0
                for i in range(0, itr):
                    new_size = n + size
                    print("Insert to DB", [n, new_size])
                    self.stat[n:new_size].to_sql(con=self.conn, name=self.__db_table, if_exists=how, index=False)
                    n += size
            print('Inserted')
        except Exception as e:
            print(e.orig)

    def __get_max_date(self):
        try:
            t = sqlalchemy.text('SELECT max(date) FROM {}'.format(self.__db_table))
            self.max_date = pd.to_datetime(self.conn.execute(t).fetchall()[0])
        except Exception as e:
            print(e.orig)


adfox = Adfox()
adfox.getData(filling=True)
# adfox.getData(from_date='2021-10-23', to_date='2021-10-25')
adfox.pushData()
