import yaml
import pandas as pd
import requests
import sqlalchemy
from time import sleep
from math import ceil
import datetime
from tqdm import tqdm


class Adwile():
    def __init__(self, file_config='../config/config.yaml'):
        self.loadConfig(file_config)

        self.__set_conn()
        self.__get_max_date()
        self.__url_campaigns = 'https://cabinet.adwile.com/webmaster/v1/campaigns?sort_order=asc'
        self.__url_informers = 'https://cabinet.adwile.com/webmaster/v2/informers'
        self.__url_informers_stat = 'https://cabinet.adwile.com/webmaster/v2/informer/stat'

    def __setBlockTypes(self, names):
        turbo = []
        amp = []
        florad = []
        mobile = []
        desktop = []
        stick = []
        fullscreen = []
        video = []
        side = []
        main = []

        for item in names:
            item = item.lower()
            if ('турбо' in item) or ('turbo' in item):
                turbo.append(True)
            else:
                turbo.append(False)
            if ('amp' in item) or ('амп' in item):
                amp.append(True)
            else:
                amp.append(False)
            if ('floor' in item) or ('floar' in item):
                florad.append(True)
            else:
                florad.append(False)
            if (':m' in item) or ('mobile' in item) or ('мобил' in item):
                mobile.append(True)
            else:
                mobile.append(False)
            if (':d' in item) or ('desk' in item):
                desktop.append(True)
            else:
                desktop.append(False)
            if ('stick' in item):
                stick.append(True)
            else:
                stick.append(False)
            if ('fullsc' in item):
                fullscreen.append(True)
            else:
                fullscreen.append(False)
            if ('video' in item):
                video.append(True)
            else:
                video.append(False)
            if ('side' in item):
                side.append(True)
            else:
                side.append(False)
            if ('main' in item):
                main.append(True)
            else:
                main.append(False)

        df = pd.DataFrame(list(zip(turbo, amp, florad, mobile, desktop, stick,
                                   fullscreen, video, side, main)),
                          columns=['turbo', 'amp', 'florad', 'mobile', 'desktop',
                                   'stick', 'fullscreen', 'video', 'side', 'main'])

        return df

    def getData(self,
                from_date=(datetime.date.today() - datetime.timedelta(days=1)),
                to_date=(datetime.date.today()),
                filling=False
                ):

        if pd.to_datetime(from_date) > pd.to_datetime(to_date):
            if self.max_date < pd.to_datetime(to_date):
                from_date = pd.to_datetime(self.max_date).strftime('%Y-%m-%d')[0]

        # Нужно ли наполнять от последней даты
        if filling:
            # если пусто, то берем за вчера
            self.__get_max_date()
            if pd.isnull(self.max_date[0]) == False:
                from_date = self.max_date[0].strftime('%Y-%m-%d')

            to_date = (datetime.date.today())

            # реализовать различные варианты накатывания обновлений

        filters = '&group_by=date'

        url = ''.join((self.__url_campaigns + filters).split())

        data = []
        informers = pd.DataFrame()
        for account in self.__accounts:
            good_response = False
            i = 1
            while good_response == False:
                r = requests.get(self.__url_campaigns, headers={'Authorization': account['token']})
                if r.status_code != 200:
                    print('Error request campaign ({}-10): {} - {}'.format(i, r.status_code, r.content.decode('utf-8')))
                    if (i >= 10):
                        print('No response campaign')
                        exit()
                    sleep(10)
                    i += 1
                else:
                    data.append({'account': account['login'], 'data': r.json()})
                    good_response = True

            good_response = False
            i = 1
            while good_response == False:
                r = requests.get(self.__url_informers, headers={'Authorization': account['token']})
                if r.status_code != 200:
                    print('Error request campaign ({}-10): {} - {}'.format(i, r.status_code, r.content.decode('utf-8')))
                    if (i >= 10):
                        print('No response campaign')
                        exit()
                    sleep(10)
                    i += 1
                else:
                    df = pd.DataFrame(r.json()['informers']).reset_index(drop=False)
                    df['account'] = account['login']
                    chunks = [informers, df]
                    informers = pd.concat(chunks, ignore_index=True)
                    good_response = True

        campaigns = pd.json_normalize(informers['campaign'])
        campaigns.columns = ['compaign_id', 'compaign_name']
        campaigns.reset_index(drop=False, inplace=True)

        del informers['campaign']

        informers.rename(columns={'id': 'informer_id', 'name': 'informer_name'}, inplace=True)
        informers['informer_name'] = [str(i).lower() for i in informers['informer_name']]
        informers = informers.copy().merge(campaigns)
        del informers['index']

        templates = []
        for i in informers['templates']:
            if len(i) > 0:
                templates.append(i[0])
            else:
                templates.append({'id': None, 'name': None})
        templates = pd.DataFrame([templates]).T
        templates = pd.json_normalize(templates[0])
        templates.columns = ['template_id', 'template_name']
        templates['template_name'] = [str(i).lower() for i in templates['template_name']]
        templates.reset_index(drop=False, inplace=True)
        informers.reset_index(drop=False, inplace=True)
        informers = informers.copy().merge(templates)
        del informers['index']
        del informers['templates']

        informers = informers.copy().join(self.__setBlockTypes(informers['informer_name']))
        informers.sort_values(['compaign_id', 'informer_id'], inplace=True)
        informers['compaign_name'] = informers['compaign_name'].str.replace('\(WM\) ', '', regex=True)

        data_list = []
        counter = 0

        for informer_id, account in tqdm(zip(informers['informer_id'], informers['account']), leave=True,
                                         desc='Load informers data',
                                         total=len(list(zip(informers['informer_id'], informers['account'])))):

            for acc in self.__accounts:
                if acc['login'] == account:
                    token = acc['token']
            r = requests.get(self.__url_informers_stat, headers=({
                "Authorization": token}),
                             params=({
                                 'informer_id': informer_id,
                                 # 'campaign_id':campaign,
                                 'date_from': from_date,
                                 'date_to': to_date,
                                 'group_by': 'date',
                                 'detail': 'day'
                             }))
            if r.status_code != 200:
                print('Error request informers stat ({}-10): {} - {}'.format(i, r.status_code,
                                                                             r.content.decode('utf-8')))
                if (i >= 10):
                    print('No response informers stat')
                    exit()
                sleep(10)
                i += 1
            else:
                if len(r.json()['stat']) > 0:
                    counter += 1
                    if 'stat' in r.json():
                        data_list.append([informer_id, r.json()['stat']])

        if len(data_list) == 0:
            print('No data informers stat')
            exit()
        adwile = pd.DataFrame()
        for i in data_list:
            informer_id = i[0]
            data = i[1]
            for item in data:
                item = item['stat']
                item = pd.json_normalize(item)
                item['informer_id'] = informer_id
                chunks = [adwile, item]
                adwile = pd.concat(chunks, ignore_index=True)
        if 'key_display_value' in adwile.columns:
            del adwile['key_display_value']
        adwile.rename(columns={'key': 'date'}, inplace=True)
        adwile['date'] = pd.to_datetime(adwile['date'])

        adwile = adwile.copy().merge(informers, on=['informer_id'], how='left')

        adwile['informer_name'] = adwile['informer_name'].astype('category')
        adwile.rename({"compaign_name": "domain"}, inplace=True, axis=1)
        adwile['domain'] = adwile['domain'].astype('category')
        adwile['template_name'] = adwile['template_name'].astype('category')
        adwile.drop_duplicates(inplace=True)
        adwile.sort_values(by=['date'], ascending=False, inplace=True)
        adwile.reset_index(drop=True, inplace=True)
        self.stat = adwile
        print('Loaded')

    def loadConfig(self, file_config):
        with open(file_config, 'r') as stream:
            try:
                cfg = yaml.safe_load(stream)
                self.__db_user = cfg['DB_PG']['user']
                self.__db_pass = cfg['DB_PG']['pass']
                self.__db_host = cfg['DB_PG']['host']
                self.__db_db = cfg['DB_PG']['db']
                self.__db_table = cfg['Adwile']['db_table']
                accounts = cfg['Adwile']['accounts']
                self.__accounts = []
                for account in accounts.keys():
                    self.__accounts.append({'login': account, 'token': accounts[account]['token']})
            except Exception as exc:
                print(exc)

    def __set_conn(self):
        try:
            self.conn = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                                 format(self.__db_user,
                                                        self.__db_pass,
                                                        self.__db_host,
                                                        self.__db_db))
        except Exception as e:
            print(e.orig)

    def __get_max_date(self):
        try:
            t = sqlalchemy.text('SELECT max(date) FROM {}'.format(self.__db_table))
            self.max_date = pd.to_datetime(self.conn.execute(t).fetchall()[0])
        except Exception as e:
            print(e.orig)

    def pushData(self):
        try:
            size = 15000
            how = 'append'

            if len(self.stat) <= size:
                self.stat.to_sql(con=self.conn, name=self.__db_table, if_exists=how, index=False)
            else:
                itr = ceil(len(self.stat) / size)
                n = 0
                for i in range(0, itr):
                    new_size = n + size
                    print("Insert to DB", [n, new_size])
                    self.stat[n:new_size].to_sql(con=self.conn, name=self.__db_table, if_exists=how, index=False)
                    n += size
            print('Inserted')
        except Exception as e:
            print(e.orig)


adwile = Adwile()

adwile.getData(filling=True)
#adwile.stat.to_csv('1.csv', sep=';', encoding='cp1251')
adwile.pushData()
