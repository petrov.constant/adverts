import pandas as pd
import requests
import json
import sqlalchemy
from tqdm import tqdm
import yaml
from math import ceil
import datetime



class YandexRTB():
    def __init__(self, file_config='./config/config.yaml'):
        self.qVars1 = [
            {"lang": "ru"},
            {"pretty": 1},
            {"dimension_field": "date|day"},
            # {"entity_field":"page_id"},
            # {"entity_field":'page_caption'},
            {"entity_field": "complex_block_id"},
            # {"entity_field":"block_caption"},
            # {"entity_field":"design_name"},
            # {"entity_filter_fields":"block_caption"},
            {"field": "shows"},
            {"field": "shows_own"},
            {"field": "hits"},
            {"field": "hits_own"},
            {"field": "hits_unsold"},
            {"field": "hits_render"},
            {"field": "visibility"},
            {"field": "fillrate"},
            {"field": "winrate"},
            {"field": "ecpm_partner_wo_nds"},
            {"field": "rpm_partner_wo_nds"},
            {"field": "cpmv_partner_wo_nds"},
            {"field": "partner_wo_nds"},
            {"order_by": '[{"field":"date","dir":"asc"}]'}]

        self.qVars2 = [
            {"lang": "ru"},
            {"pretty": 1},
            {"entity_field": "block_caption"},
            {"entity_field": "complex_block_id"},
            {"entity_field": "page_id"},
            {"entity_field": 'page_caption'},
            {"field": "hits"}]


        self.url = 'https://partner2.yandex.ru/api/statistics2/get.json?'
        self.loadConfig(file_config)
        self.__set_conn()
        self.__get_max_date()

    def loadConfig(self, file_config):
        with open(file_config, 'r') as stream:
            try:
                cfg = yaml.safe_load(stream)
                self.__db_user = cfg['DB_PG']['user']
                self.__db_pass = cfg['DB_PG']['pass']
                self.__db_host = cfg['DB_PG']['host']
                self.__db_db = cfg['DB_PG']['db']
                self.__db_table = cfg['Yandex']['db_table']
                self.__token = cfg['Yandex']['token']
            except Exception as exc:
                print(exc)



    def getData(self,
                from_date=(datetime.date.today() - datetime.timedelta(days=1)),
                to_date=(datetime.date.today()),
                filling=False
                ):

        if pd.to_datetime(from_date) > pd.to_datetime(to_date):
            if self.max_date < pd.to_datetime(to_date):
                from_date = pd.to_datetime(self.max_date).strftime('%Y-%m-%d')[0]

        def refresh_period(var, from_date, to_date):
            from_date=pd.to_datetime(from_date).strftime('%Y-%m-%d')
            to_date=pd.to_datetime(to_date).strftime('%Y-%m-%d')
            todel=[]
            for i in range(len(var)):
                if 'period' in var[i].keys():
                    todel.append(i)
            if len(todel)>0:
                for i in sorted(todel, reverse=True):
                    del var[i]
            var.append({'period':from_date})
            var.append({'period':to_date})

        #Нужно ли наполнять от последней даты
        if filling:
            #если пусто, то берем за вчера
            self.__get_max_date()
            if pd.isnull(self.max_date[0])==False:
                from_date=self.max_date[0]

            to_date = (datetime.date.today())

        refresh_period(self.qVars1, from_date, to_date)
        refresh_period(self.qVars2, from_date, to_date)




        # generate url for data
        url = self.url
        for var in self.qVars1:
            for key, value in var.items():
                url += '&' + str(key) + '=' + str(value)

        try:
            req = requests.get(url, headers={'Authorization': self.__token})
            data = json.loads(req.content)['data']
            print('total_rows:', data['total_rows'])
        except Exception as e:
            print(e)
            print(req.content.decode('utf8'))

        df = pd.DataFrame()

        dimensions = []
        for dimension in data['dimensions'].keys():
            dimensions.append(dimension)

        measures = []
        for measure in data['measures'].keys():
            measures.append(measure)

        for line in tqdm(data['points'], desc="Restructure"):
            raw = {}
            for dimension in dimensions:
                raw[dimension] = line['dimensions'][dimension]
            for measure in measures:
                raw[measure] = line['measures'][0][measure]

            if 'date' in raw:
                raw['date'] = raw['date'][0]
            if (len(df) == 0) & (len(raw) > 0):
                df = pd.DataFrame(raw, index=[0])
            else:
                raw = pd.DataFrame(raw, index=[0])
                chunks = [df, raw]
                df = pd.concat(chunks, ignore_index=True)

        self.stat = df
        del df
        self.stat['date'] = pd.to_datetime(self.stat['date'], format='%Y-%m-%d')
        self.stat['block_id'] = self.stat['complex_block_id'].astype('category')

        url = self.url
        for var in self.qVars2:
            for key, value in var.items():
                url += '&' + str(key) + '=' + str(value)

        req = requests.get(url, headers={'Authorization': self.__token})
        data = json.loads(req.content)['data']
        print('total_rows:', data['total_rows'])

        blocks = pd.DataFrame()

        dimensions = []
        for dimension in data['dimensions'].keys():
            dimensions.append(dimension)

        measures = []
        for measure in data['measures'].keys():
            measures.append(measure)

        for line in tqdm(data['points'], desc="Restructure"):
            raw = {}
            for dimension in dimensions:
                raw[dimension] = line['dimensions'][dimension]
            for measure in measures:
                raw[measure] = line['measures'][0][measure]

            if (len(blocks) == 0) & (len(raw) > 0):
                blocks = pd.DataFrame(raw, index=[0])
            else:
                raw = pd.DataFrame(raw, index=[0])
                chunks = [blocks, raw]
                blocks = pd.concat(chunks, ignore_index=True)

        del blocks['hits']
        blocks = blocks.drop_duplicates()
        blocks.rename(columns={'complex_block_id': 'block_id'}, inplace=True)
        self.stat = self.stat.merge(blocks, on=['block_id'])
        self.stat['domain'] = [str(i).split()[0] for i in self.stat['page_caption']]
        self.stat['block_caption'] = [i.lower() for i in self.stat['block_caption']]
        self.__setBlockTypes(self.stat['block_caption'])
        self.stat['page_caption'] = self.stat['page_caption'].astype('category')
        self.stat['block_id'] = self.stat['block_id'].astype('category')
        self.stat['page_id'] = self.stat['page_id'].astype('category')
        del self.stat['complex_block_id']
        self.stat['block_caption'] = self.stat['block_caption'].astype('category')
        self.stat['domain'] = self.stat['domain'].astype('category')
        self.stat.rename({"block_caption": "block_name"}, axis='columns', inplace=True)

        if (pd.to_datetime(from_date) <= self.max_date):
            t = sqlalchemy.text('DELETE FROM {} WHERE date>=\'{}\''.format(self.__db_table, str(from_date)))
            ##дописать
            self.conn.execute(t)
            # реализовать различные варианты накатывания обновлений

    def __setBlockTypes(self, names):
        turbo = []
        amp = []
        florad = []
        mobile = []
        desktop = []
        stick = []
        fullscreeen = []
        video = []
        side = []
        main =[]


        for item in names:
            item = item.lower()
            if ('турбо' in item) or ('turbo' in item):
                turbo.append(True)
            else:
                turbo.append(False)
            if ('amp' in item) or ('амп' in item):
                amp.append(True)
            else:
                amp.append(False)
            if ('floor' in item) or ('floar' in item):
                florad.append(True)
            else:
                florad.append(False)
            if (':m' in item) or ('mobile' in item) or ('мобил' in item):
                mobile.append(True)
            else:
                mobile.append(False)
            if (':d' in item) or ('desk' in item):
                desktop.append(True)
            else:
                desktop.append(False)
            if ('stick' in item):
                stick.append(True)
            else:
                stick.append(False)
            if ('fullsc' in item):
                fullscreeen.append(True)
            else:
                fullscreeen.append(False)
            if ('video' in item):
                video.append(True)
            else:
                video.append(False)
            if ('side' in item):
                side.append(True)
            else:
                side.append(False)
            if ('main' in item):
                main.append(True)
            else:
                main.append(False)

        df = pd.DataFrame(list(zip(turbo, amp, florad, mobile, desktop, stick,
                                   fullscreeen, video, side, main)),
                          columns=['turbo', 'amp', 'florad', 'mobile', 'desktop',
                                   'stick', 'fullscreeen', 'video', 'side','main'])

        self.stat = self.stat.join(df)

    def to_csv(self, file='YandexRTB.csv'):
        self.stat.to_csv(file, encoding='cp1251', sep=';')

    def __set_conn(self):
        try:
            self.conn = sqlalchemy.create_engine('postgresql://{0}:{1}@{2}/{3}'.
                                                           format(self.__db_user,
                                                                  self.__db_pass,
                                                                  self.__db_host,
                                                                  self.__db_db))
        except Exception as e:
            print(e.orig)


    def pushData(self):
        try:
            size = 15000
            how = 'append'

            if len(self.stat) <= size:
                self.stat.to_sql(con=self.conn, name=self.__db_table, if_exists=how, index=False)
            else:
                itr = ceil(len(self.stat) / size)
                n = 0
                for i in range(0, itr):
                    new_size = n + size
                    print("Insert to DB", [n, new_size])
                    self.stat[n:new_size].to_sql(con=self.conn, name=self.__db_table, if_exists=how, index=False)
                    n += size
            print('Inserted')
        except Exception as e:
            print(e.orig)

    def __get_max_date(self):
        try:
            t=sqlalchemy.text('SELECT max(date) FROM {}'.format(self.__db_table))
            self.max_date=pd.to_datetime(self.conn.execute(t).fetchall()[0])
        except Exception as e:
            print(e.orig)





Ya = YandexRTB()
Ya.getData(filling=True)
Ya.pushData()
